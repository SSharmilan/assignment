package overloading;

public class calculateArea {
	public void square()
	 { 
	System.out.println("No Parameter Method Called");
	 } 
	public void square( int number )
	 {
	int square = number * number;
	System.out.println("Method with Integer Argument Called:"+square);
	 
	}

	public void square(double number) {
		double square = number * number;
		 System.out.println("Method with double Argument Called:"+square);
		
	}
}
