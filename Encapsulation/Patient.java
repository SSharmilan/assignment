package Encapsulation;
public class Patient {

	private String name;
	private int age;
	private int pid;
	//get method age to access the private variable age
	public int getage()
	{
		return age;
	}
	public int getpid()
	{
		return pid;
	}
	public String getname()
	{
		return name;
	}
	//setter method
	public void setage(int a) {
		this.age=a;
	}
	public void setpid(int id) {
		this.pid=id;
	}
	public void setname(String n)
	{
		this.name=n;
	}
}
