package polimophism;

public class Circle extends Shape {
	private double radion;
	public Circle(double r)
	{
		this.radion=r;
	}
	public double getarea()
	{
		return 3.14*radion*radion;
	}
	public double getparameter()
	{
		return 2*3.14*radion;
	}

}
