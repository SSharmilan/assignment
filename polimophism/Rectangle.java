package polimophism;

public class Rectangle extends Shape {
	private double width;
	private double length;
	public Rectangle(double w,double l)
	{
		this.width=w;
		this.length=l;
	}
	public double getarea() {
		return width*length;
	}
	public double getparameter()
	{
		return 2*width*length;
	}

}
