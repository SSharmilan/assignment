package Hiearchical_Inheritance;

public class Shape {


	private double width;
	private double length;
	private double radius;
	
	public double cirarea(double radius) {
		this.radius=radius;
		
		return 3.14*radius*radius;
	}
	
	public double recarea(double width,double length) {
		this.length=length;
		this.width=width;
		return length*width;
	}
}
