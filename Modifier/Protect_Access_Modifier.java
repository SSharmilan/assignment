
package Modifier;
//A-&gt;B-&gt;C = class hierarchy
class A 
{ 
   protected void display() 
    { 
        System.out.println("hiii"); 
    } 
} 
 
class B extends A {}  
class C extends B {}
public class Protect_Access_Modifier {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		  B obj = new B();     //create object of class B   
	       obj.display();       //access class A protected method using obj
	       C cObj = new C();    //create object of class C
	       cObj.display ();     //access class A protected method using cObj
	}

}
